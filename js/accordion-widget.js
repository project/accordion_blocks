(function($, Drupal, drupalSettings) {
    Drupal.behaviors.accordion_blocks = {
        attach: function (context, settings) {
            const accordion_settings = drupalSettings.accordion_blocks;
            $('.accordion_blocks_container', context).accordion({header: "h2.accordion-title", heightStyle: accordion_settings.heightStyle ?? false, collapsible: accordion_settings.collapsible });
        }
    }
})(jQuery, Drupal, drupalSettings);
